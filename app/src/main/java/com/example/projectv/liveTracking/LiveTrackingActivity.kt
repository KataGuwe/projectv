package com.example.projectv.liveTracking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.projectv.R
import kotlinx.android.synthetic.main.activity_live_tracking.*

class LiveTrackingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_tracking)

        imageView4.setOnClickListener {
            onBackPressed()
        }

        User()
    }

    private fun User(){
        imageView9.tag = 0
        imageView9.setOnClickListener {
            if(imageView9.tag == 0) {
                tracking_destination_card.visibility = View.VISIBLE
                imageView9.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp)
                imageView9.tag = 1
            }else {
                tracking_destination_card.visibility = View.GONE
                imageView9.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp)
                imageView9.tag = 0
            }
        }
    }
}
