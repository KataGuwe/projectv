package com.example.projectv.order

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.example.projectv.R
import kotlinx.android.synthetic.main.activity_order.*
import java.util.*
import kotlin.concurrent.schedule

class OrderActivity : AppCompatActivity() {
    private lateinit var myDialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)

        imageView3.setOnClickListener {
            onBackPressed()
        }

        editText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        editText2.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        editText3.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        editText4.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        editText5.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        button.setOnClickListener {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            button.visibility = View.GONE
            progress_bar_order.visibility = View.VISIBLE
            Timer("OrderCar", false).schedule(500) {
                runOnUiThread {
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                }
                verifyDialog()
            }
        }
    }

    private fun verifyDialog(){
        myDialog = Dialog(this@OrderActivity)
        myDialog.setContentView(R.layout.popup_order_success)
        myDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        myDialog.setOnDismissListener {
            onBackPressed()
        }
        myDialog.show()
        myDialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun hide(v: View) {
        val input = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        input.hideSoftInputFromWindow(v.windowToken, 0)
    }
}
