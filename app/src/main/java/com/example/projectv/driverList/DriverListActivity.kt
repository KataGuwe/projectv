package com.example.projectv.driverList

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.projectv.R
import kotlinx.android.synthetic.main.activity_driver_list.*

class DriverListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_list)

        val driverList = arrayOf("Sugus", "Andri", "Sudu", "Politeknik", "Bagus", "Sinta")
        rvDriverList.layoutManager = LinearLayoutManager(this@DriverListActivity, RecyclerView.VERTICAL, false)
        rvDriverList.adapter = DriverListAdapter(driverList)

        imageView5.setOnClickListener {
            onBackPressed()
        }
    }
}
