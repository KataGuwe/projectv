package com.example.projectv.driverList

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projectv.R
import kotlinx.android.synthetic.main.list_driver.view.*

class DriverListAdapter(private val DriverList: Array<String>) : RecyclerView.Adapter<DriverListViewHolder>() {
    override fun onBindViewHolder(holder: DriverListViewHolder, position: Int) {
        holder.bind(DriverList[position], position)
    }

    override fun getItemCount() = DriverList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_driver, parent, false)
        return DriverListViewHolder(view)
    }
}

class DriverListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
    private var view: View = itemView
    private var Driver: String? = null

    override fun onClick(p0: View?) {

    }

    init {
        itemView.setOnClickListener(this)
    }

    fun bind(Driver: String, pos: Int) {
        this.Driver = Driver
        view.textView9.text = Driver
        if(pos == 3 || pos == 5) {
            view.view8.setBackgroundResource(R.color.color_notready)
        }
        if(pos == 0 || pos == 4) {
            view.view8.setBackgroundResource(R.color.color_booked)
        }
    }
}