package com.example.projectv

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.projectv.driverList.DriverListActivity
import com.example.projectv.liveTracking.LiveTrackingActivity
import com.example.projectv.order.OrderActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       main_order_btn.setOnClickListener {
           order()
       }

        main_driver_btn.setOnClickListener {
            driver()
        }

        main_tracking_btn.setOnClickListener {
            tracking()
        }

    }

    private fun order(){
        val intent = Intent(this@MainActivity, OrderActivity::class.java)
        startActivity(intent)
    }

    private fun driver(){
        val intent = Intent(this@MainActivity, DriverListActivity::class.java)
        startActivity(intent)
    }

    private fun tracking(){
        val intent = Intent(this@MainActivity, LiveTrackingActivity::class.java)
        startActivity(intent)
    }
}