package com.example.projectv

import android.animation.Animator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.OvershootInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import com.daasuu.ei.Ease
import com.daasuu.ei.EasingInterpolator
import com.flaviofaria.kenburnsview.RandomTransitionGenerator
import kotlinx.android.synthetic.main.activity_login.*
import android.graphics.drawable.TransitionDrawable
import android.view.inputmethod.InputMethodManager
import java.util.*
import kotlin.concurrent.schedule

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mainEditText1.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        mainEditText2.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if(!hasFocus) {
                hide(v)
            }
        }

        val generator = RandomTransitionGenerator(20000, AccelerateDecelerateInterpolator())
        fragmentloginKenBurnsView1.setTransitionGenerator(generator)
        imageView2.animate().setStartDelay(1000).setDuration(2000).alpha(1f).start()
        fragmentloginView1.animate().setStartDelay(1000).setDuration(2000).alpha(0.6f).start()
        button_login.animate().translationX((resources.displayMetrics.widthPixels + button_login.measuredWidth).toFloat()).setDuration(0).setStartDelay(0).start()
        button_login.animate().translationX(0f).setStartDelay(2000).setDuration(1500).setInterpolator(OvershootInterpolator()).start()
        form_login.animate().translationY(resources.displayMetrics.heightPixels.toFloat()).setStartDelay(0).setDuration(0).start()
        form_login.animate().translationY(0f).setDuration(1500).alpha(1f).setStartDelay(2000).start()
        val va = ValueAnimator()
        va.duration = 1000
        va.interpolator = DecelerateInterpolator()
        va.addUpdateListener { p1 ->
            val button_login_lp = button_login.layoutParams as ConstraintLayout.LayoutParams
            button_login_lp.width = Math.round(p1.animatedValue as Float)
            button_login.layoutParams = button_login_lp
        }
        button_login.tag = 0
        button_login.setOnClickListener(View.OnClickListener {
            if (button_login.tag as Int == 1) {
                return@OnClickListener
            } else if (button_login.tag as Int == 2) {
                button_login.animate().x((resources.displayMetrics.widthPixels / 2).toFloat()).y((resources.displayMetrics.heightPixels / 2).toFloat())
                    .setInterpolator(EasingInterpolator(Ease.CUBIC_IN)).setListener(null).setDuration(500)
                    .setStartDelay(0).start()
                button_login.animate().setStartDelay(600).setDuration(1000).scaleX(40f).scaleY(40f)
                    .setInterpolator(EasingInterpolator(Ease.CUBIC_IN_OUT)).start()
                button_login.animate().alpha(0f).rotation(90f).setStartDelay(0).setDuration(800).start()
                return@OnClickListener
            }
            button_login.tag = 1
            va.setFloatValues(button_login.measuredWidth.toFloat(), button_login.measuredHeight.toFloat())
            va.start()
            mainProgressBar1.animate().setStartDelay(300).setDuration(1000).alpha(1f).start()
            button_label.animate().setStartDelay(100).setDuration(500).alpha(0f).start()
            button_login.animate().setInterpolator(FastOutSlowInInterpolator()).setStartDelay(4000).setDuration(1000)
                .scaleX(30f).scaleY(30f).setListener(object :
                    Animator.AnimatorListener {
                    override fun onAnimationStart(p1: Animator) {
                        val transition = button_login.background as TransitionDrawable
                        transition.startTransition(500)
                        mainProgressBar1.animate().setStartDelay(0).setDuration(0).alpha(0f).start()
                        Timer("SettingUp", false).schedule(500) {
                            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                            finish()
                        }
                    }

                    override fun onAnimationEnd(p1: Animator) {
                    }

                    override fun onAnimationCancel(p1: Animator) {
                    }

                    override fun onAnimationRepeat(p1: Animator) {
                    }
                }).start()
        })


    }

    private fun hide(v: View) {
        val input = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        input.hideSoftInputFromWindow(v.windowToken, 0)
    }
}
